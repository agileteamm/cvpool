package com.cvpool.common.util;

public interface Constants {
    String empty = "";
    String space = " ";
    String slash = "/";
    String hyphen = "-";
    String comma = ",";
    String dat = ".";
    String percentage = "%";
    String reagent = " | ";

    String FILE_PREFIX = "cv";
    String DIRECTION_DESC = "DESC";
    String CREATED_DATE = "createdDate";
    String LOGIN_WITH_REF = "/giris-yap?ref=";
    String REDIRECT_POST_AD_CAT_SELECT = "redirect:/ilan-ver/kategori-secim";
    String REDIRECT_UPDATE_PHONE_NUMBER = "redirect:/uyelik-bilgilerim?ref=aday";
    String REDIRECT_POST_AD_AD_INFO = "redirect:/ilan-ver/ilan-bilgileri";
    String REDIRECT_ADMIN_PANEL = "redirect:/admin/panelim";
    String DEFAULT_PASSWORD = "haraP";
    String OG_TITLE = "ogTitle";

    String STEP_KATEGORI_SECIM = "kategori-secim";
    String STEP_ILAN_BILGILERI = "ilan-bilgileri";
    String STEP_ODEME_YAP = "odeme-yap";
    String STEP_SONUC = "sonuc";
}