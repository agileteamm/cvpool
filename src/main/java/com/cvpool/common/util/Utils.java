package com.cvpool.common.util;

import com.cvpool.common.dao.entity.IdEntity;
import com.cvpool.domain.dto.SearchDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Random;

import static com.cvpool.common.util.Constants.STEP_ILAN_BILGILERI;
import static com.cvpool.common.util.Constants.STEP_KATEGORI_SECIM;
import static com.cvpool.common.util.Constants.STEP_ODEME_YAP;
import static com.cvpool.common.util.Constants.STEP_SONUC;

@Slf4j
@Component
public class Utils {

    public static ObjectMapper mapper = new ObjectMapper();

    public static SearchDto getProperties(Map<String, String> params) {
        params.keySet().removeIf(p -> in(p, "page", "size", "sort", "direction", "id", "searchText",
                "status", "date", "city", "district", "userId", "intervew", "fullname", "minSalary"));
        return SearchDto.builder().properties(params).build();
    }

    public static String changeOrder(String str, String part) {
        if (StringUtils.isNotEmpty(part) && StringUtils.isNotEmpty(part) && !str.startsWith(part)) {
            str = part.concat(Constants.comma + str).replace(Constants.comma + part, Constants.empty);
        }
        return str;
    }

    public static void createPath(Path root) throws IOException {
        if (!Files.isDirectory(root)) {
            Files.createDirectories(root);
        }
    }

    public static String hashMD5(String userName, String password) {
        return DigestUtils.md5DigestAsHex((StringUtils.lowerCase(userName) + password).getBytes());
    }

    private static String replaceTurkishChars(String str) {
        String result = "";
        if (StringUtils.isNotBlank(str)) {
            result = str.replaceAll("Ç", "C").replaceAll("ç", "c").replaceAll("Ö", "O").replaceAll("ö", "o")
                    .replaceAll("ı", "i").replaceAll("İ", "I").replaceAll("ş", "s").replaceAll("Ş", "S")
                    .replaceAll("Ğ", "G").replaceAll("ğ", "g").replaceAll("Ü", "U").replaceAll("ü", "u");
        }
        return result;
    }

    private static String replaceSpecialChars(String str) {
        String result = "";
        if (StringUtils.isNotBlank(str)) {
            result = str.replaceAll(" ", "-").replaceAll("\\+", "-").replaceAll("/", "-").replaceAll("\\?", "")
                    .replaceAll("\\'", "");
        }
        return result;
    }

    public static int random() {
        return (int) (Math.random() * 100);
    }

    public static String uniqueFileName(String fileName) {
        return Utils.normalizeCharacters(System.currentTimeMillis() + random() + fileName.substring(fileName.lastIndexOf(Constants.dat)));
    }

    public static String normalizeCharacters(String str) {
        String result = "";
        if (StringUtils.isNotBlank(str)) {
            result = Utils.replaceSpecialChars(Utils.replaceTurkishChars(str.toLowerCase()));
        }
        return result;
    }

    public static void redirect(HttpServletResponse response, int status, String url) {
        response.setStatus(status);
        response.setHeader("Location", url);
        response.setHeader("Connection", "close");
    }

    public static <T> T call(String url, Class<T> responseType) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Connect-Type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<T> response = restTemplate.postForEntity(url, entity, responseType);
            return response.getBody();
        } catch (Exception e) {
            log.error("An exception occured on Service call ", e);
        }
        return null;
    }

    public static boolean in(String obj, String... list) {
        if (StringUtils.isEmpty(obj)) {
            return false;
        }
        for (String item : list) {
            if (obj.equals(item)) {
                return true;
            }
        }
        return false;
    }

    public static boolean notIn(String obj, String... list) {
        return !in(obj, list);
    }

    public static <T extends IdEntity> T checkEntity(T entity) {
        return (entity == null || entity.getIdentifier() == null) ? null : entity;
    }

    public static String getDeviceType(Device device) {
        String deviceType = null;
        if (device.isMobile()) {
            deviceType = "mobile";
        } else if (device.isTablet()) {
            deviceType = "tablet";
        } else {
            deviceType = "desktop";
        }
        return deviceType;
    }

    public static boolean isValidPostAdStep(String step) {
        return STEP_KATEGORI_SECIM.equals(step)
                || STEP_ILAN_BILGILERI.equals(step)
                || STEP_ODEME_YAP.equals(step)
                || STEP_SONUC.equals(step);
    }

    public static String generateRandomText(int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}
