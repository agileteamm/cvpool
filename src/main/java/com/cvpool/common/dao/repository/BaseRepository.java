package com.cvpool.common.dao.repository;

import com.cvpool.common.enumeration.EntityStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@NoRepositoryBean
public interface BaseRepository<T, P extends Serializable> extends JpaRepository<T, P>, JpaSpecificationExecutor<T> {
    class QueryGeneration {
        public static List<Predicate> getPredicateArray() {
            return new ArrayList<Predicate>() {
                @Override
                public boolean add(Predicate predicate) {
                    if (predicate != null) {
                        return super.add(predicate);
                    }
                    return false;
                }
            };
        }

        public static <S> List<S> aggregate(EntityManager em, CriteriaQuery<S> query, Root<?> root, CriteriaBuilder cb, Predicate predicate,
                                            List<Selection<?>> selectionList, List<Expression<?>> groupByList, LockModeType lockMode) {
            if (!CollectionUtils.isEmpty(selectionList)) {
                if (predicate != null) {
                    query.where(predicate);
                }
                query.multiselect(selectionList);
                if (!CollectionUtils.isEmpty(groupByList)) {
                    query.groupBy(groupByList);
                }
                return em.createQuery(query).setLockMode(lockMode).getResultList();
            }
            return null;
        }

        public static Predicate idPredicate(CriteriaBuilder cb, Root<?> root, String id) {
            return equalPredicate(cb, root, "identifier", id);
        }

        public static Predicate idsPredicate(CriteriaBuilder cb, Root<?> root, Set<String> ids) {
            return inPredicate(cb, root, "identifier", ids);
        }

        public static Predicate nameLikePredicate(CriteriaBuilder cb, Root<?> root, String name) {
            return likePredicate(cb, root, "name", name);
        }

        public static Predicate statusPredicate(CriteriaBuilder cb, Root<?> root, EntityStatus status) {
            return status == null ? null : cb.equal(root.get("status"), status);
        }
        
        public static Predicate statusNotPredicate(CriteriaBuilder cb, Root<?> root, EntityStatus status) {
            return status == null ? null : cb.notEqual(root.get("status"), status);
        }

        public static Predicate usernamePredicate(CriteriaBuilder cb, Root<?> root, String username) {
            return equalPredicate(cb, root, "username", username);
        }

        public static Predicate equalPredicate(CriteriaBuilder cb, Root<?> root, String fieldName, String fieldValue) {
            return StringUtils.isEmpty(fieldValue) ? null : cb.equal(root.get(fieldName), fieldValue);
        }

        public static Predicate equalPredicate(CriteriaBuilder cb, Root<?> root, String fieldName, Long fieldValue) {
            return StringUtils.isEmpty(fieldValue) ? null : cb.equal(root.get(fieldName), fieldValue);
        }

        public static Predicate equalBooleanPredicate(CriteriaBuilder cb, Root<?> root, String fieldName, Boolean fieldValue) {
            return StringUtils.isEmpty(fieldValue) ? null : cb.equal(root.get(fieldName), fieldValue);
        }

        public static Predicate gePredicate(CriteriaBuilder cb, Root<?> root, String fieldName, Integer fieldValue) {
            return StringUtils.isEmpty(fieldValue) ? null : cb.greaterThanOrEqualTo(root.get(fieldName), fieldValue);
        }

        public static Predicate geDatePredicate(CriteriaBuilder cb, Root<?> root, String fieldName, Integer fieldValue) {
            return StringUtils.isEmpty(fieldValue) ? null : cb.greaterThanOrEqualTo(root.get(fieldName), ZonedDateTime.now().minusDays(fieldValue));
        }

        public static Predicate likePredicate(CriteriaBuilder cb, Root<?> root, String fieldName, String fieldValue) {
            return StringUtils.isEmpty(fieldValue) ? null : cb.like(cb.upper(root.get(fieldName)), "%" + fieldValue.toUpperCase() + "%");
        }

        public static Predicate inPredicate(CriteriaBuilder cb, Root<?> root, String fieldName, Set<String> fieldValues) {
            return CollectionUtils.isEmpty(fieldValues) ? null : cb.in(root.get(fieldName)).value(fieldValues);
        }

        public static Predicate notInPredicate(CriteriaBuilder cb, Root<?> root, String fieldName, Set<String> fieldValues) {
            return CollectionUtils.isEmpty(fieldValues) ? null : cb.not(cb.in(root.get(fieldName)).value(fieldValues));
        }
    }
}
