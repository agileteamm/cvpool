package com.cvpool.common.enumeration;

public enum ChannelType {
    FORM, FACEBOOK, GOOGLE, ADMIN_FORM;
}
