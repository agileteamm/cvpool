package com.cvpool.common.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MailType {
    CANDIDATE_CREATE("Tebrikler, %s nolu cv oluşturuldu.", "mail/candidate-create", EntityStatus.ACTIVE),
    CANDIDATE_UPDATE("Tebrikler, %s nolu cv güncellendi.", "mail/candidate-update", EntityStatus.ACTIVE),
    CANDIDATE_DELETE("%s nolu cv silinmiştir", "mail/candidate-delete", EntityStatus.DELETED),
    REGISTRATION("CV Havuzu Ailesine Hoş Geldiniz", "mail/registration", EntityStatus.DEFAULT),
    PASSWORD_FORGOT("Cv havuzu - Şifremi unuttum", "mail/password-forgot", EntityStatus.DEFAULT),
    PASSWORD_CHANGE("Cv havuzu - Şifre Güncelleme", "mail/password-change", EntityStatus.DEFAULT),
    CREATE_NEW_PASSWORD("Cv havuzu - Yeni Şifre Oluşturma", "mail/create-new-password", EntityStatus.DEFAULT);

    private String subject;
    private String template;
    private EntityStatus entityStatus;

    public String getSubject(String candidateNo) {
        return String.format(this.subject, candidateNo);
    }
}
