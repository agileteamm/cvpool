package com.cvpool.common.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SmsType {
    PASSWORD_FORGOT("Yeni şifre belirlemek için tıklayınız.");

    private String text;

}
