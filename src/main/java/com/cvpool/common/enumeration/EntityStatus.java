package com.cvpool.common.enumeration;

public enum EntityStatus {
    DEFAULT, ACTIVE, DELETED
}
