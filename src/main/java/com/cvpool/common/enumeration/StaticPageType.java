package com.cvpool.common.enumeration;

import lombok.Getter;

import java.util.stream.Stream;

@Getter
public enum StaticPageType {
    ABOUT(Path.ABOUT, "about"),
    CONTACT(Path.CONTACT, "contact"),
    SERVICES(Path.SERVICES, "services"),
    MEMBERSHIP_AGGREMENT(Path.MEMBERSHIP_AGGREMENT, "membership-aggrement"),
    BANNED_PRODUCT_LIST(Path.BANNED_PRODUCT_LIST, "banned-product-list"),
    TERMS_OF_USE(Path.TERMS_OF_USE, "term-of-use"),
    PRIVACY_POLICY(Path.PRIVACY_POLICY, "privacy-policy"),
    KVKK(Path.KVKK, "kvkk"),
    HELP(Path.HELP, "help"),
    ERROR(Path.ERROR, Path.ERROR);

    private String path;
    private String page;

    StaticPageType(String path, String page) {
        this.path = path;
        this.page = "static/" + page;
    }

    public static String getPage(String path) {
        return StaticPageType.stream()
                .filter(p -> p.getPath().equals(path))
                .findAny()
                .get()
                .getPage();
    }

    public static void append(StringBuilder builder) {
        stream().forEach(p -> builder.append(",/".concat(p.getPath())));
    }

    public static Stream<StaticPageType> stream() {
        return Stream.of(StaticPageType.values());
    }

    public interface Path {
        String ABOUT = "hakkimizda";
        String CONTACT = "iletisim";
        String SERVICES = "hizmetlerimiz";
        String MEMBERSHIP_AGGREMENT = "uyelik-sozlesmesi";
        String TERMS_OF_USE = "kullanim-kosullari";
        String PRIVACY_POLICY = "gizlilik";
        String KVKK = "kvkk";
        String HELP = "yardim";
        String BANNED_PRODUCT_LIST = "yasakli-urunler-listesi";
        String ERROR = "error";
    }
}