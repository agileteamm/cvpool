package com.cvpool.common.enumeration;

public enum PropertyType {
    text,
    number,
    checkbox,
    date,
    select,
    radio,
    yesNo;
}
