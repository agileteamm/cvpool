package com.cvpool.common.enumeration;

import com.cvpool.domain.dao.entity.Candidate;
import com.cvpool.domain.dao.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

import static com.cvpool.common.enumeration.EntityStatus.ACTIVE;
import static com.cvpool.common.enumeration.EntityStatus.DEFAULT;
import static com.cvpool.common.enumeration.EntityStatus.DELETED;

@Getter
@AllArgsConstructor
public enum NotificationType {
    CANDIDATE_CREATE(ACTIVE, ACTIVE, MailType.CANDIDATE_CREATE, null, Candidate.class),
    CANDIDATE_UPDATE(ACTIVE, ACTIVE, MailType.CANDIDATE_UPDATE, null, Candidate.class),
    CANDIDATE_DELETE(DEFAULT, DELETED, MailType.CANDIDATE_DELETE, null, Candidate.class),
    REGISTRATION(DEFAULT, DEFAULT, MailType.REGISTRATION, null, User.class),
    PASSWORD_CHANGE(DEFAULT, DEFAULT, MailType.PASSWORD_CHANGE, null, User.class),
    CREATE_NEW_PASSWORD(DEFAULT, DEFAULT, MailType.CREATE_NEW_PASSWORD, null, User.class),
    PASSWORD_FORGOT(DEFAULT, DEFAULT, MailType.PASSWORD_FORGOT, SmsType.PASSWORD_FORGOT, User.class);

    private EntityStatus oldStatus;
    private EntityStatus status;
    private MailType mailType;
    private SmsType smsType;
    private Class clazz;

    public static NotificationType of(EntityStatus oldStatus, EntityStatus status, Class clazz) {
        return Arrays.stream(NotificationType.values())
                .filter(s -> (s.oldStatus.equals(DEFAULT) || s.oldStatus.equals(oldStatus)) && s.status.equals(status) && s.clazz.equals(clazz))
                .findFirst()
                .orElse(null);
    }
}
