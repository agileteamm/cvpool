package com.cvpool.common.mapper;

import com.cvpool.common.controller.request.BaseRequest;
import com.cvpool.common.controller.response.BaseResponse;
import com.cvpool.common.dao.entity.IdEntity;

public interface BaseMapper<Entity extends IdEntity, Request extends BaseRequest, Response extends BaseResponse> {

    Response toResponse(Entity entity);

    Entity toEntity(Request request);
    
}
