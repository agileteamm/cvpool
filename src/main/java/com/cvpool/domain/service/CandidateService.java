package com.cvpool.domain.service;

import com.cvpool.common.enumeration.EntityStatus;
import com.cvpool.common.exception.exceptions.MicroException;
import com.cvpool.common.util.Utils;
import com.cvpool.domain.dao.entity.Candidate;
import com.cvpool.domain.dao.entity.CandidateProperty;
import com.cvpool.domain.dao.entity.Media;
import com.cvpool.domain.dao.repository.CandidatePropertyRepository;
import com.cvpool.domain.dao.repository.CandidateRepository;
import com.cvpool.domain.dto.NotificationDto;
import com.cvpool.domain.dto.SearchDto;
import com.cvpool.domain.dto.SearchResultDto;
import com.cvpool.domain.service.notification.NotificationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class CandidateService {
    public static final String DEFAULT_TITLE = "CV";
    private final CandidateRepository candidateRepository;
    private final CandidatePropertyRepository candidatePropertyRepository;
    private final SecurityService securityService;
    private final NotificationService notificationService;
    private final CategoryService categoryService;

    static AtomicLong candidateNo;

    @PostConstruct
    private void init() {
        candidateNo = new AtomicLong(candidateRepository.findMaxCandidateNo().orElseGet(() -> 1L));
    }

    public Candidate get(String id) {
        return candidateRepository.findById(id)
                .orElseThrow(() -> MicroException.newDataNotFoundException(Candidate.class.getSimpleName(), id));
    }

    public Candidate get(Long candidateNo) {
        return candidateRepository.findByCandidateNo(candidateNo).orElseThrow(
                () -> MicroException.newDataNotFoundException(Candidate.class.getSimpleName(), candidateNo.toString()));
    }

    @Transactional
    @CacheEvict(value = "candidate", allEntries = true)
    public Candidate put(String id, Candidate candidate) {
        Candidate theReal = candidateRepository.findById(id)
                .orElseThrow(() -> MicroException.newDataNotFoundException(Candidate.class.getSimpleName(), id));
        EntityStatus status = theReal.getStatus();
        theReal.setTitle(StringUtils.isNotEmpty(candidate.getTitle()) ? candidate.getTitle() : DEFAULT_TITLE);
        theReal.setDescription(candidate.getDescription());
        theReal.setCity(candidate.getCity());
        theReal.setDistrict(candidate.getDistrict());
        theReal.setSearchText(Utils.normalizeCharacters(theReal.getTitle()));
        if (candidate.getCategory() != null) {
            theReal.setCategory(candidate.getCategory());
        }
        Candidate updatedCandidate = candidateRepository.save(theReal);
        notificationService.notify(NotificationDto.builder().oldStatus(status).entity(updatedCandidate).build());
        return updatedCandidate;
    }

    @Transactional
    @CacheEvict(value = "candidate", allEntries = true)
    public void delete(String id) {
        Candidate theReal = candidateRepository.findById(id)
                .orElseThrow(() -> MicroException.newDataNotFoundException(Candidate.class.getSimpleName(), id));
        candidateRepository.delete(theReal);
    }

    @Transactional
    @CacheEvict(value = "candidate", allEntries = true)
    public Candidate save(Candidate candidate) {
        candidate.setUser(securityService.getLoginUser());
        candidate.setTitle(StringUtils.isNotEmpty(candidate.getTitle()) ? candidate.getTitle() : DEFAULT_TITLE);
        candidate.setSearchText(Utils.normalizeCharacters(candidate.getTitle()));
        candidate.setStatus(EntityStatus.ACTIVE);
        candidate.setCandidateNo(candidateNo.incrementAndGet());
        candidate.setMedia(Media.builder().build());
        return candidateRepository.save(candidate);
    }

    public Map<String, List<SearchResultDto>> searchCandidateForCategoryGroup(SearchDto searchDto) {
        Map<String, SearchResultDto> categoriesMap = new HashMap<>();
        categoryService.getParentCategories().stream()
                .forEach(c -> c.getChildren()
                        .forEach(child -> categoriesMap.put(child.getSearchText(),
                                SearchResultDto.builder()
                                        .parentName(c.getName())
                                        .categorySearchText(child.getSearchText())
                                        .categoryName(child.getName())
                                        .build())));

        search(searchDto, PageRequest.of(0, 1000)).get()
                .forEach(a -> {
                    String cathSearchText = a.getCategory().getSearchText();
                    categoriesMap.get(cathSearchText).setAdCount(categoriesMap.get(cathSearchText).getAdCount() + 1L);
                });

        Map<String, List<SearchResultDto>> searchResultList = new HashMap<>();
        categoriesMap.entrySet().stream().filter(e -> e.getValue().getAdCount() > 0).forEach(e -> {
            List<SearchResultDto> list = searchResultList.getOrDefault(e.getValue().getParentName(), new ArrayList<>());
            list.add(e.getValue());
            searchResultList.put(e.getValue().getParentName(), list);
        });
        return searchResultList;
    }

    @Cacheable(value = "candidate")
    public Page<Candidate> search(SearchDto searchDto, Pageable pageable) {
        return candidateRepository.findAll(CandidateRepository.QueryGeneration.search(searchDto), pageable);
    }

    @Transactional
    public void saveProperty(String id, List<CandidateProperty> properties) {
        Candidate candidate = get(id);
        List<CandidateProperty> existProperties = getCandidateProperties(candidate);
        properties.forEach(p -> {
            existProperties.forEach(e -> {
                if (e.getProperty().getIdentifier().equals(p.getProperty().getIdentifier())) {
                    p.setIdentifier(e.getIdentifier());
                }
            });
            p.setCandidate(candidate);
        });
        candidatePropertyRepository.saveAll(properties);
    }

    public List<CandidateProperty> getCandidateProperties(Candidate candidate) {
        return candidatePropertyRepository.findByCandidate(candidate).orElseGet(ArrayList::new).stream()
                .sorted(Comparator.comparingLong(p -> p.getProperty().getOrderId())).collect(Collectors.toList());
    }

    @Transactional
    @CacheEvict(value = "candidate", allEntries = true)
    public void updateStatus(String id, EntityStatus status) {
        Candidate theReal = candidateRepository.findById(id)
                .orElseThrow(() -> MicroException.newDataNotFoundException(Candidate.class.getSimpleName(), id));
        EntityStatus oldStatus = theReal.getStatus();
        theReal.setStatus(status);
        Candidate candidate = candidateRepository.save(theReal);
        if (securityService.isAdmin()) {
            notificationService.notify(NotificationDto.builder().oldStatus(oldStatus).entity(candidate).build());
        }
    }

    @Transactional
    @CacheEvict(value = "candidate", allEntries = true)
    public void updateCategory(String id, String categoryId) {
        Candidate theReal = candidateRepository.findById(id)
                .orElseThrow(() -> MicroException.newDataNotFoundException(Candidate.class.getSimpleName(), id));
        theReal.setCategory(categoryService.get(categoryId));
        candidateRepository.save(theReal);
    }
}
