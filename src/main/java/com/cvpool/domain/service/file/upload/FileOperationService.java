package com.cvpool.domain.service.file.upload;

import com.cvpool.common.exception.exceptions.MicroException;
import com.cvpool.common.util.Constants;
import com.tinify.Tinify;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.cvpool.common.util.Utils.createPath;

@Slf4j
@Service
@RequiredArgsConstructor
public class FileOperationService {

    @Value("${system.parameter.file.root}")
    String fileRoot;

    @Value("${system.parameter.file.compress.key:RmX3SR6CqVZSpk9jD9JSWDjgpw29sFFQ}")
    String compressKey;

    @PostConstruct
    private void init() {
        Tinify.setKey(compressKey);
    }

    private Path getRoot(String path) {
        return Paths.get(fileRoot + path);
    }

    public void upload(Path inFile, String path, String fileName) {
        try {
            Path root = getRoot(path);
            createPath(root);
            Files.copy(inFile, root.resolve(fileName));
        } catch (Exception e) {
            throw new MicroException(101L, "Could not store file. Error: " + e.toString());
        }
    }

    public void upload(String path, MultipartFile file, String fileName) {
        try {
            Path root = getRoot(path);
            createPath(root);
            Files.copy(file.getInputStream(), root.resolve(fileName));
        } catch (Exception e) {
            throw new MicroException(101L, "Could not store file. Error: " + e);
        }
    }

    public void compress(String path, String fileName) {
        try {
            String filePath = getRoot(path).resolve(fileName).toString();
            Tinify.fromFile(filePath).toFile(filePath);
        } catch (IOException e) {
            log.error("Could not copress file. Error: " + e);
        }
    }

    @Async
    public void deleteFile(String path, String fileName) {
        try {
            Path root = getRoot(path);
            Files.walk(root, 1)
                    .filter(p -> !p.equals(root))
                    .filter(p -> p.getFileName().toString().contains(fileName.substring(0, fileName.lastIndexOf(Constants.dat))))
                    .forEach(p -> {
                        try {
                            Files.deleteIfExists(p);
                        } catch (IOException e) {
                            log.error("An exception occured on deleting file and ignored. ", e);
                        }
                    });
        } catch (IOException e) {
            log.error("An exception occured when walking on files and ignored. ", e);
        }
    }
}