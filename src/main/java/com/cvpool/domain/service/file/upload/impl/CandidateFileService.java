package com.cvpool.domain.service.file.upload.impl;

import com.cvpool.common.util.Constants;
import com.cvpool.domain.dao.entity.Candidate;
import com.cvpool.domain.dao.repository.CandidateRepository;
import com.cvpool.domain.service.CandidateService;
import com.cvpool.domain.service.file.upload.FileOperationService;
import com.cvpool.domain.service.file.upload.IFileUploadService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import static com.cvpool.common.util.Constants.FILE_PREFIX;

@Slf4j
@Service
@RequiredArgsConstructor
public class CandidateFileService implements IFileUploadService {

    private final CandidateService candidateService;
    private final CandidateRepository candidateRepository;
    private final FileOperationService fileOperationService;

    @Override
    public void upload(String id, MultipartFile file, String fileName) {
        Candidate candidate = candidateService.get(id);
        fileOperationService.upload(candidate.getMediaPath(), file, fileName);
    }

    @Override
    @Transactional
    public void save(String candidateId, String fileName) {
        Candidate candidate = candidateService.get(candidateId);
        candidate.getMedia().setFileUrl(getImageUrl(candidate, fileName));
        candidate.getMedia().setMedia(fileName);
        candidateRepository.save(candidate);
    }

    @Override
    @Transactional
    public void changeDefault(String candidateId, String fileName) {
        // never change
    }

    @Override
    @Transactional
    public void delete(String candidateId, String fileName) {
        Candidate candidate = candidateService.get(candidateId);
        fileOperationService.deleteFile(candidate.getMediaPath(), fileName);
        candidate.getMedia().setMedia(null);
        candidate.getMedia().setFileUrl(null);
        candidateRepository.save(candidate);
    }

    @Override
    @Transactional
    public String get(String candidateId) {
        Candidate candidate = candidateService.get(candidateId);
        return candidate.getMedia().getFileUrl();
    }

    private String getImageUrl(Candidate candidate, String fileName) {
        return Constants.slash + FILE_PREFIX + candidate.getMediaPath() + Constants.slash + fileName;
    }
}