package com.cvpool.domain.service.file.upload;

import org.springframework.web.multipart.MultipartFile;

public interface IFileUploadService {

    void upload(String id, MultipartFile file, String fileName);

    void save(String id, String files);

    void changeDefault(String id, String fileName);

    void delete(String id, String fileName);

    String get(String id);
}
