package com.cvpool.domain.service.file.upload;

import com.cvpool.domain.service.file.upload.impl.CandidateFileService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FileUploadFactory {

    private final CandidateFileService candidateFileService;

    public IFileUploadService of(String criteria) {
        return candidateFileService;
    }
}
