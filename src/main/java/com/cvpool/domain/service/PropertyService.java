package com.cvpool.domain.service;

import com.cvpool.common.exception.exceptions.MicroException;
import com.cvpool.common.util.Utils;
import com.cvpool.domain.controller.response.PropertyResponse;
import com.cvpool.domain.dao.entity.Category;
import com.cvpool.domain.dao.entity.Property;
import com.cvpool.domain.dao.repository.PropertyRepository;
import com.cvpool.domain.mapper.PropertyMapper;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class PropertyService {
    private final PropertyRepository propertyRepository;
    private final CategoryService categoryService;
    private final PropertyMapper propertyMapper;

    @Cacheable(value = "property")
    public Property get(String id) {
        return propertyRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Property.class.getSimpleName(), id));
    }

    @Cacheable(value = "property")
    public List<Property> getByCategoryId(String categoryId) {
        Category category = categoryService.get(categoryId);
        return propertyRepository.findByCategoryOrderByOrderId(category).orElse(new ArrayList<>());
    }

    @Transactional
    @CacheEvict(value = "property", allEntries = true)
    public Property put(String id, Property property) {
        Property theReal = propertyRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Property.class.getSimpleName(), id));
        property.setIdentifier(theReal.getIdentifier());
        property.setSearchText(Utils.normalizeCharacters(property.getName()));
        property.setParent(Utils.checkEntity(property.getParent()));
        return propertyRepository.save(property);
    }

    @Transactional
    @CacheEvict(value = "property", allEntries = true)
    public void delete(String id) {
        Property theReal = propertyRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Property.class.getSimpleName(), id));
        propertyRepository.delete(theReal);
    }

    @Transactional
    @CacheEvict(value = "property", allEntries = true)
    public Property save(Property property) {
        property.setSearchText(Utils.normalizeCharacters(property.getName()));
        property.setParent(Utils.checkEntity(property.getParent()));
        return propertyRepository.save(property);
    }

    public List<PropertyResponse> getByCategoryIdWithLookup(String categoryId) {
        List<PropertyResponse> properties = propertyMapper.toResponse(getByCategoryId(categoryId));
        return properties;
    }
}
