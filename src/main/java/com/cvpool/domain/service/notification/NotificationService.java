package com.cvpool.domain.service.notification;

import com.cvpool.common.enumeration.NotificationType;
import com.cvpool.domain.dao.entity.Candidate;
import com.cvpool.domain.dao.entity.User;
import com.cvpool.domain.dto.NotificationDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@Slf4j
@AllArgsConstructor
public class NotificationService {

    EmailSenderService emailSenderService;

    public void notify(NotificationDto context) {
        NotificationType type = context.getType();
        if (context.getEntity() instanceof User) {
            User user = (User) context.getEntity();
            if (type.getMailType() != null) {
                emailSenderService.sendEmail(NotificationUtils.prepareMail(type.getMailType(), user));
            }
        } else {
            Candidate candidate = (Candidate) context.getEntity();
            type = context.getType() != null ? context.getType() :
                    NotificationType.of(context.getOldStatus(), candidate.getStatus(), Candidate.class);
            if (type != null) {
                if (type.getMailType() != null) {
                    emailSenderService.sendEmail(NotificationUtils.prepareMail(type.getMailType(), candidate));
                }
            }
        }
    }
}