package com.cvpool.domain.service.notification;

import com.cvpool.common.enumeration.MailType;
import com.cvpool.domain.dao.entity.Candidate;
import com.cvpool.domain.dao.entity.User;
import com.cvpool.domain.dto.MailDto;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class NotificationUtils {

    public static String baseUrl;
    public static String email;

    public static MailDto prepareMail(MailType type, User user) {
        MailDto mailDto = new MailDto();
        mailDto.setMailTo(user.getEmail());
        mailDto.setTemplate(type.getTemplate());
        mailDto.setSubject(type.getSubject());
        mailDto.setProps(getModel(user));
        return mailDto;
    }

    public static MailDto prepareMail(MailType type, Candidate candidate) {
        MailDto mailDto = new MailDto();
        User user = candidate.getUser();
        mailDto.setMailTo(user.getEmail());
        mailDto.setTemplate(type.getTemplate());
        mailDto.setSubject(type.getSubject(candidate.getCandidateNo().toString()));
        Map<String, Object> model = getModel(candidate);
        mailDto.setProps(model);
        return mailDto;
    }

    private static Map<String, Object> getModel(Candidate candidate) {
        Map<String, Object> model = getModel(candidate.getUser());
        model.put("candidateId", candidate.getIdentifier());
        model.put("candidateNo", candidate.getCandidateNo());
        model.put("title", candidate.getTitle());
        model.put("url", candidate.getUrl());
        return model;
    }

    private static Map<String, Object> getModel(User user) {
        Map<String, Object> model = getModel();
        model.put("id", user.getIdentifier());
        model.put("email", user.getEmail());
        model.put("firstName", user.getFirstName());
        model.put("lastName", user.getLastName());
        model.put("resetPasswordKey", user.getResetPasswordKey());
        return model;
    }

    private static Map<String, Object> getModel() {
        Map<String, Object> model = new HashMap<>();
        model.put("baseUrl", baseUrl);
        return model;
    }
}