package com.cvpool.domain.service;

import com.cvpool.common.enumeration.AuthorityType;
import com.cvpool.common.enumeration.ChannelType;
import com.cvpool.common.enumeration.EntityStatus;
import com.cvpool.domain.dao.entity.Setting;
import com.cvpool.domain.dao.entity.User;
import com.cvpool.domain.dao.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

@Slf4j
@Service
@RequiredArgsConstructor
public class InitService implements ApplicationRunner {

    private final UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Override
    @Async
    @Transactional
    public void run(ApplicationArguments args) {
        this.addUser();
    }

    private void addUser() {
        User user = getUser("recepyilmazr@gmail.com", "cvpool", "Recep", "Yılmaz","5321570810");
        User user2 = getUser("huseyinkizilbulak76@hotmail.com", "cvpool", "Hüseyin", "Kızılbulak","5070603353");
        User user3 = getUser("admin@kartezya.com", "kcvpool", "Recep", "Yılmaz","5321570810");
        userRepository.findByEmail(user.getUsername()).orElseGet(() -> userRepository.save(user));
        userRepository.findByEmail(user2.getUsername()).orElseGet(() -> userRepository.save(user2));
        userRepository.findByEmail(user3.getUsername()).orElseGet(() -> userRepository.save(user3));
        log.info("admin users added.");
    }

    private User getUser(String email, String password, String firstName, String lastName, String phoneNumber) {
        return User.builder()
                .status(EntityStatus.ACTIVE)
                .channel(ChannelType.ADMIN_FORM)
                .email(email)
                .password(passwordEncoder().encode(password))
                .firstName(firstName)
                .lastName(lastName)
                .phoneNumber(phoneNumber)
                .authorities(Arrays.asList(AuthorityType.ROLE_USER, AuthorityType.ROLE_ADMIN))
                .setting(Setting.buildDefault())
                .build();
    }
}
