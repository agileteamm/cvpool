package com.cvpool.domain.mapper;

import com.cvpool.common.mapper.BaseMapper;
import com.cvpool.domain.controller.request.CityRequest;
import com.cvpool.domain.controller.response.CityResponse;
import com.cvpool.domain.dao.entity.City;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CityMapper extends BaseMapper<City, CityRequest, CityResponse> {
}
