package com.cvpool.domain.mapper;

import com.cvpool.common.mapper.BaseMapper;
import com.cvpool.domain.controller.request.DistrictRequest;
import com.cvpool.domain.controller.response.DistrictResponse;
import com.cvpool.domain.dao.entity.District;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DistrictMapper extends BaseMapper<District, DistrictRequest, DistrictResponse> {
}
