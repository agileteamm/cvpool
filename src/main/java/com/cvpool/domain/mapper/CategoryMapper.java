package com.cvpool.domain.mapper;

import com.cvpool.common.mapper.BaseMapper;
import com.cvpool.domain.controller.request.CategoryRequest;
import com.cvpool.domain.controller.response.CategoryResponse;
import com.cvpool.domain.dao.entity.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CategoryMapper extends BaseMapper<Category, CategoryRequest, CategoryResponse> {

    @Override
    @Mappings({@Mapping(target = "parentId", source = "parent.identifier")})
    CategoryResponse toResponse(Category entity);

    @Override
    @Mappings({@Mapping(target = "parent.identifier", source = "parentId")})
    Category toEntity(CategoryRequest request);

    List<CategoryResponse> toResponse(List<Category> entity);
}
