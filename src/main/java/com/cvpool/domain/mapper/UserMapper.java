package com.cvpool.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.cvpool.common.mapper.BaseMapper;
import com.cvpool.domain.controller.request.SettingRequest;
import com.cvpool.domain.controller.request.UserRequest;
import com.cvpool.domain.controller.response.UserDetailResponse;
import com.cvpool.domain.controller.response.UserResponse;
import com.cvpool.domain.dao.entity.Setting;
import com.cvpool.domain.dao.entity.User;
import com.cvpool.domain.dto.SettingDto;

@Mapper(componentModel = "spring")
public interface UserMapper extends BaseMapper<User, UserRequest, UserResponse> {

    UserDetailResponse toDetailResponse(User user);

    SettingDto toDto(Setting entity);

    Setting toEntity(SettingRequest settingRequest);
    
    @Mapping(target = "createDate", expression = "java(entity.getCreatedDate().toLocalDate())")
    UserResponse toResponse(User entity);
}
