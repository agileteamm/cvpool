package com.cvpool.domain.mapper;

import com.cvpool.common.mapper.BaseMapper;
import com.cvpool.domain.controller.request.CandidatePropertyRequest;
import com.cvpool.domain.controller.request.CandidateRequest;
import com.cvpool.domain.controller.response.CandidateDetailResponse;
import com.cvpool.domain.controller.response.CandidateResponse;
import com.cvpool.domain.controller.response.CandidateSimpleResponse;
import com.cvpool.domain.dao.entity.Candidate;
import com.cvpool.domain.dao.entity.CandidateProperty;
import com.cvpool.domain.dao.entity.Category;
import com.cvpool.domain.dto.CandidatePropertyDto;
import com.cvpool.domain.dto.CategoryDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", imports = {UserMapper.class})
public interface CandidateMapper extends BaseMapper<Candidate, CandidateRequest, CandidateResponse> {

    @Mapping(target = "fileUrl", source = "media.fileUrl")
    @Mapping(target = "createDate", expression = "java(entity.getCreatedDate().toLocalDate())")
    CandidateResponse toResponse(Candidate entity);

    CandidateSimpleResponse toSimpleResponse(Candidate entity);

    CategoryDto toDto(Category category);

    @Mapping(target = "category.identifier", source = "category")
    Candidate toEntity(CandidateRequest request);

    @Mapping(target = "property.identifier", source = "property")
    CandidateProperty toEntity(CandidatePropertyRequest request);

    List<CandidateProperty> toCandidateProperties(List<CandidatePropertyRequest> request);

    @Mapping(target = "fileUrl", source = "media.fileUrl")
    @Mapping(target = "createDate", expression = "java(entity.getCreatedDate().toLocalDate())")
    CandidateDetailResponse toDetailResponse(Candidate entity);

    @Mapping(target = "name", source = "property.name")
    @Mapping(target = "type", source = "property.type")
    @Mapping(target = "propertyId", source = "property.identifier")
    CandidatePropertyDto toDto(CandidateProperty candidateProperty);

    List<CandidatePropertyDto> toPropertyDtos(List<CandidateProperty> candidateProperties);
}
