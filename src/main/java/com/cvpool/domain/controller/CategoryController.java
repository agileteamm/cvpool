package com.cvpool.domain.controller;

import com.cvpool.common.controller.AbstractController;
import com.cvpool.common.exception.exceptions.MicroException;
import com.cvpool.domain.controller.request.CategoryRequest;
import com.cvpool.domain.controller.response.CategoryMainPageResponse;
import com.cvpool.domain.controller.response.CategoryResponse;
import com.cvpool.domain.controller.response.PropertyResponse;
import com.cvpool.domain.mapper.CategoryMapper;
import com.cvpool.domain.service.CategoryService;
import com.cvpool.domain.service.PropertyService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/categories")
@AllArgsConstructor
public class CategoryController extends AbstractController {
    private CategoryService categoryService;
    private PropertyService propertyService;
    private CategoryMapper categoryMapper;

    @Transactional
    @GetMapping("/{id}")
    public CategoryResponse get(@PathVariable String id) throws MicroException {
        return categoryMapper.toResponse(categoryService.get(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void put(@PathVariable String id, @RequestBody @Validated CategoryRequest categoryRequest) {
        categoryService.put(id, categoryMapper.toEntity(categoryRequest));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void delete(@PathVariable String id) {
        categoryService.delete(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void save(@RequestBody @Validated CategoryRequest categoryRequest) {
        categoryService.save(categoryMapper.toEntity(categoryRequest));
    }

    @Transactional
    @GetMapping("/{id}/properties")
    public List<PropertyResponse> getProperties(@PathVariable String id) throws MicroException {
        return propertyService.getByCategoryIdWithLookup(id);
    }

    @Transactional
    @GetMapping("/")
    public List<CategoryResponse> getCategories() throws MicroException {
        return categoryMapper.toResponse(categoryService.getParentCategories());
    }

    @Transactional
    @GetMapping("/mainpage")
    public CategoryMainPageResponse getMainPageCategories() throws MicroException {
        return CategoryMainPageResponse.builder()
                .categories(categoryService.getMainpageCategories())
                .build();
    }

    @Transactional
    @GetMapping("/{id}/sub-categories")
    public List<CategoryResponse> getSubCategories(@PathVariable String id) throws MicroException {
        return categoryMapper.toResponse(categoryService.get(id).getChildren());
    }
}
