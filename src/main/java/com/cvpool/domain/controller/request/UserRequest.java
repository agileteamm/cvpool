package com.cvpool.domain.controller.request;

import com.cvpool.common.controller.request.BaseRequest;
import com.cvpool.common.enumeration.ChannelType;
import com.cvpool.common.enumeration.EntityStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UserRequest extends BaseRequest {
    private static final long serialVersionUID = 3809104814337239544L;
    private ChannelType channel;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String password;
    private EntityStatus status;
}
