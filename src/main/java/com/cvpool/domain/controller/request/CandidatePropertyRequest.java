package com.cvpool.domain.controller.request;

import com.cvpool.common.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CandidatePropertyRequest extends BaseRequest {
    private static final long serialVersionUID = 3809104814337239544L;
    private String property;
    private String value;
}
