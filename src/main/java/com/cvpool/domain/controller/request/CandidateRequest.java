package com.cvpool.domain.controller.request;

import com.cvpool.common.controller.request.BaseRequest;
import com.cvpool.common.enumeration.EntityStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class CandidateRequest extends BaseRequest {
	private static final long serialVersionUID = 3809104814337239544L;
	@NotNull
	private String category;
	private EntityStatus status;
	private String fullname;
	private Boolean interview;
	private Integer minSalary;
	private String title;
	private String description;
	private String city;
	private String district;
}
