package com.cvpool.domain.controller;

import com.cvpool.common.controller.AbstractController;
import com.cvpool.common.exception.exceptions.MicroException;
import com.cvpool.domain.controller.request.PropertyRequest;
import com.cvpool.domain.controller.response.PropertyResponse;
import com.cvpool.domain.mapper.PropertyMapper;
import com.cvpool.domain.service.PropertyService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/properties")
@AllArgsConstructor
public class PropertyController extends AbstractController {
    private PropertyService propertyService;
    private PropertyMapper propertyMapper;

    @GetMapping("/{id}")
    public PropertyResponse get(@PathVariable String id) throws MicroException {
        return propertyMapper.toResponse(propertyService.get(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void put(@PathVariable String id, @RequestBody @Validated PropertyRequest propertyRequest) {
        propertyService.put(id, propertyMapper.toEntity(propertyRequest));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void delete(@PathVariable String id) {
        propertyService.delete(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void save(@RequestBody @Validated PropertyRequest propertyRequest) {
        propertyService.save(propertyMapper.toEntity(propertyRequest));
    }
}
