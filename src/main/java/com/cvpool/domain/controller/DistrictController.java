package com.cvpool.domain.controller;

import com.cvpool.common.controller.AbstractController;
import com.cvpool.common.dao.repository.CustomPagedResource;
import com.cvpool.common.dao.repository.CustomPagedResourceConverter;
import com.cvpool.common.exception.exceptions.MicroException;
import com.cvpool.domain.controller.request.DistrictRequest;
import com.cvpool.domain.controller.response.DistrictResponse;
import com.cvpool.domain.mapper.DistrictMapper;
import com.cvpool.domain.service.DistrictService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/districts")
@AllArgsConstructor
public class DistrictController extends AbstractController {
    private DistrictService districtService;
    private DistrictMapper districtMapper;

    @GetMapping("/{id}")
    public DistrictResponse get(@PathVariable String id) throws MicroException {
        return districtMapper.toResponse(districtService.get(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void put(@PathVariable String id, @RequestBody @Validated DistrictRequest districtRequest) {
        districtService.put(id, districtMapper.toEntity(districtRequest));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void delete(@PathVariable String id) {
        districtService.delete(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void save(@RequestBody @Validated DistrictRequest districtRequest) {
        districtService.save(districtMapper.toEntity(districtRequest));
    }

    @GetMapping("/search")
    public CustomPagedResource<List<DistrictResponse>> pageable(@RequestParam("page") int page,
                                                            @RequestParam("size") int size,
                                                            @RequestParam(required = false, defaultValue = "name") String sort,
                                                            @RequestParam(required = false, defaultValue = "ASC") String direction,
                                                            @RequestParam("cityCode") String cityCode,
                                                            @RequestParam(name = "name", required = false) String name,
                                                            @RequestParam(name = "code", required = false) String code) {
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<DistrictResponse> map = districtService.search(cityCode, name, code, pageable).map(m -> districtMapper.toResponse(m));
        return CustomPagedResourceConverter.map(map, sort, direction);
    }
}
