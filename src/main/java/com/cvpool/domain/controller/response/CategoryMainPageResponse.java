package com.cvpool.domain.controller.response;

import com.cvpool.common.controller.response.BaseResponse;
import com.cvpool.common.controller.response.LookupDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CategoryMainPageResponse extends BaseResponse {
    private List<LookupDto> categories;
}
