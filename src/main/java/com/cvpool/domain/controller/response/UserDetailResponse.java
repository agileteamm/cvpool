package com.cvpool.domain.controller.response;

import com.cvpool.common.controller.response.BaseResponse;
import com.cvpool.common.enumeration.ChannelType;
import com.cvpool.domain.dto.SettingDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UserDetailResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private ChannelType channel;
    private String email;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private SettingDto setting;
    private String resetPasswordKey;
}
