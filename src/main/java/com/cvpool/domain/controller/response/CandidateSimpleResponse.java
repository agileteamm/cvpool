package com.cvpool.domain.controller.response;

import com.cvpool.common.controller.response.BaseResponse;
import com.cvpool.common.enumeration.EntityStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class CandidateSimpleResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private Long candidateNo;
    private EntityStatus status;
    private String fullname;
    private Boolean interview;
    private Integer minSalary;
    private String title;
    private String city;
    private String district;
    private String url;
}
