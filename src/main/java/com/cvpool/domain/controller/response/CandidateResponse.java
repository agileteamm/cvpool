package com.cvpool.domain.controller.response;

import com.cvpool.common.controller.response.BaseResponse;
import com.cvpool.common.enumeration.EntityStatus;
import com.cvpool.domain.dto.CategoryDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)
public class CandidateResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private Long candidateNo;
    private EntityStatus status;
    private CategoryDto category;
    private LocalDate createDate;
    private String fullname;
    private Boolean interview;
    private Integer minSalary;
    private String title;
    private String description;
    private String city;
    private String district;
    private String url;
    private String fileUrl;
    private UserResponse user;
}
