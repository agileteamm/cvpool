package com.cvpool.domain.controller.response;

import java.time.LocalDate;

import com.cvpool.common.controller.response.BaseResponse;
import com.cvpool.common.enumeration.ChannelType;
import com.cvpool.common.enumeration.EntityStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserResponse extends BaseResponse {
	private static final long serialVersionUID = -6795494548799475942L;
	private ChannelType channel;
	private String email;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private LocalDate createDate;
	private EntityStatus status;
}
