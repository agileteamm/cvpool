package com.cvpool.domain.controller;

import com.cvpool.common.controller.AbstractController;
import com.cvpool.common.dao.repository.CustomPagedResource;
import com.cvpool.common.dao.repository.CustomPagedResourceConverter;
import com.cvpool.common.enumeration.EntityStatus;
import com.cvpool.domain.controller.request.CreateNewPasswordRequest;
import com.cvpool.domain.controller.request.PasswordRequest;
import com.cvpool.domain.controller.request.SettingRequest;
import com.cvpool.domain.controller.request.UserRequest;
import com.cvpool.domain.controller.response.UserDetailResponse;
import com.cvpool.domain.controller.response.UserResponse;
import com.cvpool.domain.dao.entity.User;
import com.cvpool.domain.dao.repository.UserRepository;
import com.cvpool.domain.dto.NotificationDto;
import com.cvpool.domain.mapper.UserMapper;
import com.cvpool.domain.service.SecurityService;
import com.cvpool.domain.service.UserService;
import com.cvpool.domain.service.notification.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.cvpool.common.enumeration.NotificationType.REGISTRATION;

@RestController
@RequestMapping(value = "/users")
@AllArgsConstructor
public class UserController extends AbstractController {
    private UserService userService;
    private UserMapper userMapper;
    private SecurityService securityService;
    private UserRepository userRepository;
    private NotificationService notificationService;

    @GetMapping("/user-info")
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public UserResponse userInfo() {
        return userMapper.toResponse(securityService.getLoginUser());
    }

    @Transactional
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public UserDetailResponse get(@PathVariable String id) {
        return userMapper.toDetailResponse(userService.get(id));
    }

    @PutMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void put(@PathVariable String id, @RequestBody @Validated UserRequest userRequest) {
        userService.put(id, userMapper.toEntity(userRequest));
    }

    @PutMapping("/{id}/setting")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void putSetting(@PathVariable String id, @RequestBody @Validated SettingRequest settingRequest) {
        userService.putSetting(id, userMapper.toEntity(settingRequest));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void delete(@PathVariable String id) {
        User user = userService.get(id);
        user.setStatus(EntityStatus.DELETED);
        userRepository.save(user);
    }

    @PostMapping("/{id}/activate")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void activate(@PathVariable String id) {
        User user = userService.get(id);
        user.setStatus(EntityStatus.ACTIVE);
        userRepository.save(user);
    }

    @PutMapping("/{id}/status/{status}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void updateStatus(@PathVariable String id, @PathVariable EntityStatus status) {
        userService.updateStatus(id, status);
    }

    @PutMapping("/{id}/phone/{phoneNumber}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void updatePhoneNumber(@PathVariable String id, @PathVariable String phoneNumber) {
        userService.updatePhoneNumber(id, phoneNumber);
    }

    @PutMapping("/{id}/password")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void changePassword(@PathVariable String id, @RequestBody @Validated PasswordRequest passwordRequest) {
        userService.changePassword(id, passwordRequest);
    }

    @GetMapping("/search")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public CustomPagedResource<List<UserResponse>> pageable(@RequestParam("page") int page,
                                                            @RequestParam("size") int size,
                                                            @RequestParam(required = false, defaultValue = "createdDate") String sort,
                                                            @RequestParam(required = false, defaultValue = "ASC") String direction,
                                                            @RequestParam(name = "id", required = false) String id,
                                                            @RequestParam(name = "email", required = false) String email,
                                                            @RequestParam(name = "firstName", required = false) String firstName,
                                                            @RequestParam(name = "lastName", required = false) String lastName,
                                                            @RequestParam(name = "phoneNumber", required = false) String phoneNumber,
                                                            @RequestParam(name = "status", required = false) EntityStatus status) {
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<UserResponse> map = userService.search(id, email, firstName, lastName, phoneNumber, status, pageable).map(m -> userMapper.toResponse(m));
        return CustomPagedResourceConverter.map(map, sort, direction);
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void save(@RequestBody @Validated UserRequest userRequest) {
        User user = userMapper.toEntity(userRequest);
        userService.save(user);
        notificationService.notify(NotificationDto.builder().type(REGISTRATION).entity(user).build());
    }

    @PutMapping("/{id}/create-new-password")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void createNewPassword(@PathVariable String id, @RequestBody @Validated CreateNewPasswordRequest passwordRequest) {
        userService.createNewPassword(id, passwordRequest);
    }

    @PostMapping("/{email}/send-forgot-password-link")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void sendForgotPasswordLink(@PathVariable String email) {
        userService.sendForgotPasswordLink(email);
    }
}
