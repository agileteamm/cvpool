package com.cvpool.domain.controller;

import com.cvpool.common.controller.AbstractController;
import com.cvpool.common.dao.repository.CustomPagedResource;
import com.cvpool.common.dao.repository.CustomPagedResourceConverter;
import com.cvpool.common.enumeration.EntityStatus;
import com.cvpool.common.exception.exceptions.MicroException;
import com.cvpool.domain.controller.request.CandidatePropertyRequest;
import com.cvpool.domain.controller.request.CandidateRequest;
import com.cvpool.domain.controller.response.CandidateDetailResponse;
import com.cvpool.domain.controller.response.CandidateResponse;
import com.cvpool.domain.controller.response.CandidateSimpleResponse;
import com.cvpool.domain.dao.entity.Candidate;
import com.cvpool.domain.dao.entity.CandidateProperty;
import com.cvpool.domain.dto.SearchDto;
import com.cvpool.domain.mapper.CandidateMapper;
import com.cvpool.domain.service.CandidateService;
import com.cvpool.domain.service.PropertyService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static com.cvpool.common.util.Utils.getProperties;

@RestController
@RequestMapping(value = "/candidates")
@AllArgsConstructor
public class CandidateController extends AbstractController {
    private CandidateService candidateService;
    private PropertyService propertyService;
    private CandidateMapper candidateMapper;

    @Transactional
    @GetMapping("/{id}")
    public CandidateDetailResponse get(@PathVariable String id) throws MicroException {
        Candidate candidate = candidateService.get(id);
        CandidateDetailResponse candidateDetailResponse = candidateMapper.toDetailResponse(candidate);
        candidateDetailResponse.setCandidatePropertyList(candidateMapper.toPropertyDtos(candidateService.getCandidateProperties(candidate)));
        return candidateDetailResponse;
    }

    @PutMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void put(@PathVariable String id, @RequestBody @Validated CandidateRequest candidateRequest) {
        candidateService.put(id, candidateMapper.toEntity(candidateRequest));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void delete(@PathVariable String id) {
        candidateService.delete(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public CandidateResponse save(@RequestBody @Validated CandidateRequest candidateRequest) {
        Candidate candidate = candidateService.save(candidateMapper.toEntity(candidateRequest));
        return candidateMapper.toResponse(candidate);
    }

    @PutMapping("/{id}/status/{status}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void updateStatus(@PathVariable String id, @PathVariable EntityStatus status) {
        candidateService.updateStatus(id, status);
    }

    @PutMapping("/{id}/category/{categoryId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void updateCategory(@PathVariable String id, @PathVariable String categoryId) {
        candidateService.updateCategory(id, categoryId);
    }

    @PostMapping("/{id}/property")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void saveProperty(@PathVariable String id, @RequestBody List<CandidatePropertyRequest> request) {
        List<CandidateProperty> properties = candidateMapper.toCandidateProperties(request);
        properties.forEach(p -> p.setSearchText(propertyService.get(p.getProperty().getIdentifier()).getSearchText()));
        candidateService.saveProperty(id, properties);
    }

    @GetMapping("/search")
    @Transactional
    public CustomPagedResource<List<CandidateSimpleResponse>> pageable(@RequestParam("page") int page,
                                                                       @RequestParam("size") int size,
                                                                       @RequestParam(required = false, defaultValue = "createdDate") String sort,
                                                                       @RequestParam(required = false, defaultValue = "DESC") String direction,
                                                                       @RequestParam(required = false, name = "date") Integer dateInterval,
                                                                       @RequestParam(required = false) String id,
                                                                       @RequestParam(required = false) Long candidateNo,
                                                                       @RequestParam(required = false) String searchText,
                                                                       @RequestParam(required = false) String city,
                                                                       @RequestParam(required = false) String district,
                                                                       @RequestParam(required = false) String userId,
                                                                       @RequestParam(required = false) String fullname,
                                                                       @RequestParam(required = false) Boolean interview,
                                                                       @RequestParam(required = false) Integer minSalary,
                                                                       @RequestParam(required = false) Map<String, String> params) {
        SearchDto searchDto = getProperties(params);
        searchDto.setId(id);
        searchDto.setCandidateNo(candidateNo);
        searchDto.setSearchText(searchText);
        searchDto.setStatus(EntityStatus.ACTIVE);
        searchDto.setDateInterval(dateInterval);
        searchDto.setCity(city);
        searchDto.setDistrict(district);
        searchDto.setUserId(userId);
        searchDto.setFullname(fullname);
        searchDto.setInterview(interview);
        searchDto.setMinSalary(minSalary);

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<CandidateSimpleResponse> map = candidateService.search(searchDto, pageable).map(m -> candidateMapper.toSimpleResponse(m));

        return CustomPagedResourceConverter.map(map, sort, direction);
    }
}