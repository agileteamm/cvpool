package com.cvpool.domain.bff;

import com.cvpool.common.controller.AbstractController;
import com.cvpool.domain.mapper.CandidateMapper;
import com.cvpool.domain.mapper.CategoryMapper;
import com.cvpool.domain.mapper.UserMapper;
import com.cvpool.domain.service.CandidateService;
import com.cvpool.domain.service.CategoryService;
import com.cvpool.domain.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@AllArgsConstructor
public class IndexController extends AbstractController {
    private CandidateService candidateService;
    private CategoryService categoryService;
    private UserService userService;
    private CandidateMapper candidateMapper;
    private CategoryMapper categoryMapper;
    private UserMapper userMapper;

    @GetMapping("/")
    @Transactional
    public String index(Model model) {
        return "login";
    }

    @GetMapping("/giris-yap")
    @Transactional
    public String loginPage(Model model, @RequestParam(required = false) String ref) {
        model.addAttribute("ref", ref);
        return "login";
    }
}