package com.cvpool.domain.bff;

import com.cvpool.common.controller.AbstractController;
import com.cvpool.common.dao.repository.CustomPagedResource;
import com.cvpool.common.dao.repository.CustomPagedResourceConverter;
import com.cvpool.common.enumeration.EntityStatus;
import com.cvpool.common.util.Constants;
import com.cvpool.domain.controller.response.CandidateResponse;
import com.cvpool.domain.dto.SearchDto;
import com.cvpool.domain.mapper.CandidateMapper;
import com.cvpool.domain.mapper.UserMapper;
import com.cvpool.domain.service.CandidateService;
import com.cvpool.domain.service.CustomLogoutHandler;
import com.cvpool.domain.service.SecurityService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
@PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
public class UserBffController extends AbstractController {

    private final SecurityService securityService;
    private final UserMapper userMapper;
    private final CandidateService candidateService;
    private final CandidateMapper candidateMapper;
    private final CustomLogoutHandler logoutHandler;

    @GetMapping("/panelim")
    public String dashboard(Model model) {

        if (securityService.isAdmin()) {
            return Constants.REDIRECT_ADMIN_PANEL;
        } else {
            SearchDto searchDto = SearchDto.builder().userId(securityService.getLoginUser().getIdentifier()).build();

            Pageable pageable = PageRequest.of(0, 1000, Sort.Direction.DESC, Constants.CREATED_DATE);
            Page<CandidateResponse> map = candidateService.search(searchDto, pageable).map(candidateMapper::toResponse);
            CustomPagedResource<List<CandidateResponse>> candidates = CustomPagedResourceConverter.map(map,
                    Constants.CREATED_DATE, Sort.Direction.DESC.toString());

            model.addAttribute("candidates", candidates);
            model.addAttribute("page", "panelim");

            return "dashboard";
        }
    }

    @GetMapping("/uyelik-bilgilerim")
    public String myAccount(Model model, @RequestParam(required = false) String ref) {
        model.addAttribute("user", userMapper.toResponse(securityService.getLoginUser()));
        model.addAttribute("ref", ref);
        return "user/my-account";
    }

    @GetMapping("/sifre-degisikligi")
    public String changePassword(Model model) {
        model.addAttribute("user", userMapper.toResponse(securityService.getLoginUser()));
        return "user/change-password";
    }

    @GetMapping("/bilgilendirme-ayarlari")
    @Transactional
    public String mySettings(Model model) {
        model.addAttribute("user", userMapper.toDetailResponse(securityService.getLoginUser()));
        return "user/my-settings";
    }

    @GetMapping(value = "/cikis-yap")
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            logoutHandler.logout(request, response, authentication);
        }
        return "redirect:/";
    }
}