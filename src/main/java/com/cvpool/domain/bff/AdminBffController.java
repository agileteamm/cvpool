package com.cvpool.domain.bff;

import com.cvpool.common.controller.AbstractController;
import com.cvpool.common.dao.repository.CustomPagedResource;
import com.cvpool.common.dao.repository.CustomPagedResourceConverter;
import com.cvpool.common.enumeration.EntityStatus;
import com.cvpool.common.util.Constants;
import com.cvpool.domain.controller.response.UserResponse;
import com.cvpool.domain.mapper.CandidateMapper;
import com.cvpool.domain.mapper.UserMapper;
import com.cvpool.domain.service.CandidateService;
import com.cvpool.domain.service.SecurityService;
import com.cvpool.domain.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping(value = "/admin")
@PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
public class AdminBffController extends AbstractController {
    private final SecurityService securityService;
    private final UserMapper userMapper;
    private final CandidateService candidateService;
    private final UserService userService;
    private final CandidateMapper candidateMapper;

    @GetMapping("/panelim")
    public String dashboard(Model model) {
        model.addAttribute("user", userMapper.toResponse(securityService.getLoginUser()));
        return "dashboard";
    }


    @Transactional
    @GetMapping("/kullanicilar")
    public String users(@RequestParam(value = "page", defaultValue = "0") int page,
                        @RequestParam(value = "size", defaultValue = "20") int size,
                        @RequestParam(defaultValue = Constants.CREATED_DATE, required = false) String sort,
                        @RequestParam(defaultValue = Constants.DIRECTION_DESC, required = false) String direction,
                        @RequestParam(name = "id", required = false) String id,
                        @RequestParam(name = "email", required = false) String email,
                        @RequestParam(name = "firstName", required = false) String firstName,
                        @RequestParam(name = "lastName", required = false) String lastName,
                        @RequestParam(name = "phoneNumber", required = false) String phoneNumber,
                        @RequestParam(name = "status", required = false) EntityStatus status, Model model) {

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<UserResponse> map = userService.search(id, email, firstName, lastName, phoneNumber, status, pageable)
                .map(userMapper::toResponse);
        CustomPagedResource<List<UserResponse>> userList = CustomPagedResourceConverter.map(map, sort, direction);
        model.addAttribute("dataList", userList);

        return "users";
    }

    @GetMapping("/kullanici")
    public String userForm(@RequestParam(name = "id", required = false) String id, Model model) {

        UserResponse user = UserResponse.builder().build();
        if (id != null) {
            user = userMapper.toResponse(userService.get(id));
        }
        model.addAttribute("user", user);
        return "users/user-form :: user-form-modal";
    }

}
