package com.cvpool.domain.bff;

import com.cvpool.common.controller.AbstractController;
import com.cvpool.common.dao.repository.CustomPagedResource;
import com.cvpool.common.dao.repository.CustomPagedResourceConverter;
import com.cvpool.common.enumeration.EntityStatus;
import com.cvpool.common.enumeration.StaticPageType;
import com.cvpool.common.exception.exceptions.MicroException;
import com.cvpool.common.util.Constants;
import com.cvpool.common.util.Utils;
import com.cvpool.domain.controller.response.CandidateDetailResponse;
import com.cvpool.domain.controller.response.CandidateResponse;
import com.cvpool.domain.controller.response.CategoryResponse;
import com.cvpool.domain.controller.response.CityResponse;
import com.cvpool.domain.dao.entity.Candidate;
import com.cvpool.domain.dao.entity.CandidateProperty;
import com.cvpool.domain.dao.entity.Category;
import com.cvpool.domain.dto.SearchDto;
import com.cvpool.domain.dto.SearchResultDto;
import com.cvpool.domain.mapper.CandidateMapper;
import com.cvpool.domain.mapper.CategoryMapper;
import com.cvpool.domain.mapper.CityMapper;
import com.cvpool.domain.service.CandidateService;
import com.cvpool.domain.service.CategoryService;
import com.cvpool.domain.service.CityService;
import com.cvpool.domain.service.PropertyService;
import com.cvpool.domain.service.SecurityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.mobile.device.Device;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.cvpool.common.util.Constants.OG_TITLE;
import static com.cvpool.common.util.Constants.STEP_ILAN_BILGILERI;
import static com.cvpool.common.util.Constants.STEP_KATEGORI_SECIM;
import static com.cvpool.common.util.Constants.slash;
import static com.cvpool.common.util.Utils.getProperties;
import static com.cvpool.common.util.Utils.isValidPostAdStep;
import static org.springframework.util.StringUtils.isEmpty;

@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping(value = "/aday")
public class CandidateBffController extends AbstractController {
    private final CandidateService candidateService;
    private final CandidateMapper candidateMapper;
    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;
    private final CityService cityService;
    private final CityMapper cityMapper;
    private final PropertyService propertyService;
    private final SecurityService securityService;

    @GetMapping("/{id}")
    @Transactional
    public String candidateDetailPage(HttpServletRequest request, Model model, @PathVariable String id) {
        try {
            Candidate candidate = candidateService.get(id);
            CandidateDetailResponse candidateDetailResponse = candidateMapper.toDetailResponse(candidate);
            List<CandidateProperty> candidateProperties = candidateService.getCandidateProperties(candidate);
            candidateDetailResponse.setCandidatePropertyList(candidateMapper.toPropertyDtos(candidateProperties));
            model.addAttribute("candidateDetail", candidateDetailResponse);

            request.setAttribute(OG_TITLE, candidate.getCategory().getName() + Constants.reagent + candidate.getTitle());
            model.addAttribute("pageName", "detail");
        } catch (Exception e) {
            return StaticPageType.Path.ERROR;
        }
        return "detail";
    }

    @Transactional
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    @GetMapping(value = {"/ekle", "/ekle/{step}", "/ekle/{step}/{id}"})
    public String postAdPage(HttpServletRequest request, Model model, Device device,
                             @PathVariable(required = false) String step,
                             @PathVariable(name = "id", required = false) String candidateId) throws Exception {

        Candidate candidate = null;

        if (securityService.getLoginUser().getIdentifier() != null
                && (securityService.getLoginUser().getPhoneNumber() == null || securityService.getLoginUser().getPhoneNumber().length() != 10)) {
            return Constants.REDIRECT_UPDATE_PHONE_NUMBER;
        }

        if (!isEmpty(candidateId)) {
            if (!isValidPostAdStep(step)) {
                return Constants.REDIRECT_POST_AD_AD_INFO + "/" + candidateId;
            }
            try {
                candidate = candidateService.get(candidateId);
                if (!securityService.isAdmin() && !candidate.getUser().getIdentifier().equals(securityService.getLoginUser().getIdentifier()))
                    return Constants.REDIRECT_POST_AD_CAT_SELECT;

                CandidateDetailResponse candidateDetailResponse = candidateMapper.toDetailResponse(candidate);
                candidateDetailResponse.setCandidatePropertyList(candidateMapper.toPropertyDtos(candidateService.getCandidateProperties(candidate)));
                model.addAttribute("candidateDetail", candidateDetailResponse);

                model.addAttribute("subCategoryList", categoryMapper
                        .toResponse(categoryService.get(candidate.getCategory().getParent().getIdentifier()).getChildren()));
                model.addAttribute("parentCategory", candidate.getCategory().getParent().getIdentifier());
            } catch (MicroException e) {
                return Constants.REDIRECT_POST_AD_CAT_SELECT;
            }
        }

        if (isEmpty(candidateId) || !isValidPostAdStep(step) || STEP_KATEGORI_SECIM.equals(step)) {
            step = STEP_KATEGORI_SECIM;
            List<CategoryResponse> categoryMap = categoryMapper.toResponse(categoryService.getParentCategories());
            model.addAttribute("categoryList", categoryMap);
        } else if (STEP_ILAN_BILGILERI.equals(step)) {
            Pageable pageable = PageRequest.of(0, 100, Sort.Direction.ASC, "name");
            Page<CityResponse> cityMap = cityService.search("TR", null, null, pageable).map(cityMapper::toResponse);
            model.addAttribute("cityList", cityMap.get().collect(Collectors.toList()));
            model.addAttribute("propertyList", propertyService.getByCategoryIdWithLookup(candidate.getCategory().getIdentifier()));
        }
        model.addAttribute("step", step);
        model.addAttribute("id", candidateId);
        model.addAttribute("candidateNo", candidate == null ? 0L : candidate.getCandidateNo());
        model.addAttribute("deviceType", Utils.getDeviceType(device));
        return "post-candidate";
    }

    @Transactional
    @GetMapping("/text-ara/{searchText}")
    public String searchByText(HttpServletRequest request, Model model, @PathVariable String searchText) {
        if (NumberUtils.isDigits(searchText)) {
            try {
                Candidate candidate = candidateService.get(Long.valueOf(searchText));
                return candidateDetailPage(request, model, candidate.getIdentifier());
            } catch (Exception e) {
                log.info("Full text search going on although candidate not found! ", e);
            }
        }

        SearchDto searchDto = SearchDto.builder()
                .status(EntityStatus.ACTIVE)
                .searchText(searchText).build();
        Map<String, List<SearchResultDto>> searchResultList = candidateService.searchCandidateForCategoryGroup(searchDto);

        Long searchCount = searchResultList.entrySet()
                .stream()
                .flatMap(s -> s.getValue().stream())
                .mapToLong(SearchResultDto::getAdCount)
                .sum();

        model.addAttribute("searchResultList", searchResultList);
        model.addAttribute("searchCriteria", "searchText");
        model.addAttribute("searchCriteriaValue", searchText);
        model.addAttribute("searchCriteriaTitle", searchText);
        model.addAttribute("searchCount", searchCount);

        return "search-by-text";
    }

    @GetMapping("ara/{categorySearchText}")
    @Transactional
    public String search(HttpServletRequest request, HttpServletResponse response, Model model, Device device,
                         @PathVariable String categorySearchText,
                         @RequestParam(value = "page", defaultValue = "0") int page,
                         @RequestParam(value = "size", defaultValue = "20") int size,
                         @RequestParam(defaultValue = Constants.CREATED_DATE, required = false) String sort,
                         @RequestParam(defaultValue = Constants.DIRECTION_DESC, required = false) String direction,
                         @RequestParam(name = "id", required = false) String id,
                         @RequestParam(name = "candidateNo", required = false) Long candidateNo,
                         @RequestParam(name = "searchText", required = false) String searchText,
                         @RequestParam(name = "date", required = false) Integer dateInterval,
                         @RequestParam(name = "city", required = false) String city,
                         @RequestParam(name = "district", required = false) String district,
                         @RequestParam Map<String, String> params) {
        SearchDto searchDto = getProperties(params);
        searchDto.setCategorySearchText(categorySearchText);
        searchDto.setPage(page);
        searchDto.setSize(size);
        searchDto.setSort(sort);
        searchDto.setDirection(direction);
        searchDto.setId(id);
        searchDto.setCandidateNo(candidateNo);
        searchDto.setSearchText(searchText);
        searchDto.setStatus(EntityStatus.ACTIVE);
        searchDto.setDateInterval(dateInterval);
        searchDto.setCity(city);
        searchDto.setDistrict(district);

        try {
            Category category = categoryService.getBySearchText(searchDto.getCategorySearchText());
            consolideSearchResult(model, device, category, searchDto);
            model.addAttribute("propertyList", propertyService.getByCategoryIdWithLookup(category.getIdentifier()));
            model.addAttribute("categoryPathMap", getCategoryMap(category));

            request.setAttribute(OG_TITLE, category.getName());

            model.addAttribute("categorySearcText", searchDto.getCategorySearchText());

        } catch (MicroException e) {
            Utils.redirect(response, HttpServletResponse.SC_MOVED_PERMANENTLY, slash);
            return null;
        } catch (Exception e) {
            log.error("İlanlar getirilirken hata oluştu. categorySearchText: {} ", searchDto.getCategorySearchText(), e);
            return StaticPageType.Path.ERROR;
        }
        return "search";
    }

    private void consolideSearchResult(Model model, Device device, Category category, SearchDto searchDto) {
        Pageable pageable = PageRequest.of(searchDto.getPage(), searchDto.getSize(), Sort.Direction.valueOf(searchDto.getDirection()), searchDto.getSort());

        Page<CandidateResponse> map = candidateService.search(searchDto, pageable).map(candidateMapper::toResponse);
        CustomPagedResource<List<CandidateResponse>> candidateResource = CustomPagedResourceConverter.map(map, searchDto.getSort(), searchDto.getDirection());
        model.addAttribute("candidateResource", candidateResource);

        List<CategoryResponse> categoryList = categoryMapper.toResponse(categoryService.getParentCategories());
        model.addAttribute("categoryList", categoryList);

        Pageable pageableCity = PageRequest.of(0, 100, Sort.Direction.ASC, "name");
        Page<CityResponse> cityMap = cityService.search("TR", null, null, pageableCity).map(cityMapper::toResponse);
        model.addAttribute("cityList", cityMap.get().collect(Collectors.toList()));

        model.addAttribute("filterParams", searchDto);
        model.addAttribute("pageName", "search");
        model.addAttribute("deviceType", Utils.getDeviceType(device));
    }

    private Map<String, String> getCategoryMap(Category category) {
        Map<String, String> categoryPathMap = new HashMap<>();
        if (category.getParent() != null) {
            categoryPathMap.put(category.getParent().getSearchText(), category.getParent().getName());
        }
        categoryPathMap.put(category.getSearchText(), category.getName());
        return categoryPathMap;
    }
}