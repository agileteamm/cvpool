package com.cvpool.domain.bff;

import com.cvpool.common.controller.AbstractController;
import com.cvpool.common.enumeration.StaticPageType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AllArgsConstructor
public class StaticPageController extends AbstractController {

    @GetMapping(StaticPageType.Path.ERROR)
    public String error() {
        return StaticPageType.Path.ERROR;
    }

    @GetMapping(StaticPageType.Path.ABOUT)
    public String about() {
        return StaticPageType.ABOUT.getPage();
    }

    @GetMapping(StaticPageType.Path.CONTACT)
    public String contact() {
        return StaticPageType.CONTACT.getPage();
    }

    @GetMapping(StaticPageType.Path.SERVICES)
    public String services() {
        return StaticPageType.SERVICES.getPage();
    }

    @GetMapping(StaticPageType.Path.MEMBERSHIP_AGGREMENT)
    public String membershipAggrement() {
        return StaticPageType.MEMBERSHIP_AGGREMENT.getPage();
    }

    @GetMapping(StaticPageType.Path.BANNED_PRODUCT_LIST)
    public String bannedProductList() {
        return StaticPageType.BANNED_PRODUCT_LIST.getPage();
    }

    @GetMapping(StaticPageType.Path.TERMS_OF_USE)
    public String termOfUse() {
        return StaticPageType.TERMS_OF_USE.getPage();
    }

    @GetMapping(StaticPageType.Path.PRIVACY_POLICY)
    public String privacyPolicy() {
        return StaticPageType.PRIVACY_POLICY.getPage();
    }

    @GetMapping(StaticPageType.Path.KVKK)
    public String kvkk() {
        return StaticPageType.KVKK.getPage();
    }

    @GetMapping(StaticPageType.Path.HELP)
    public String help() {
        return StaticPageType.HELP.getPage();
    }
}