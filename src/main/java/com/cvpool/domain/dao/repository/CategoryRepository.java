package com.cvpool.domain.dao.repository;

import com.cvpool.common.dao.repository.BaseRepository;
import com.cvpool.domain.dao.entity.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends BaseRepository<Category, String> {
    Optional<List<Category>> findByParent(Category parent);

    Optional<Category> findBySearchText(String searchText);

}
