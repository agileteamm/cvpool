package com.cvpool.domain.dao.repository;

import com.cvpool.common.dao.repository.BaseRepository;
import com.cvpool.domain.dao.entity.Media;
import org.springframework.stereotype.Repository;

@Repository
public interface MediaRepository extends BaseRepository<Media, String> {
}