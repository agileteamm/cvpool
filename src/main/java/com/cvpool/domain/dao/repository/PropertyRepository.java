package com.cvpool.domain.dao.repository;

import com.cvpool.common.dao.repository.BaseRepository;
import com.cvpool.domain.dao.entity.Category;
import com.cvpool.domain.dao.entity.Property;

import java.util.List;
import java.util.Optional;

public interface PropertyRepository extends BaseRepository<Property, String> {

    Optional<List<Property>> findByCategoryOrderByOrderId(Category category);
}
