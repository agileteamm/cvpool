package com.cvpool.domain.dao.repository;

import com.cvpool.common.dao.repository.BaseRepository;
import com.cvpool.common.util.Constants;
import com.cvpool.domain.dao.entity.Candidate;
import com.cvpool.domain.dao.entity.CandidateProperty;
import com.cvpool.domain.dao.entity.User;
import com.cvpool.domain.dto.SearchDto;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface CandidateRepository extends BaseRepository<Candidate, String> {

    Optional<Candidate> findByCandidateNo(Long candidateNo);

    @Query(value = "select max(candidate_no) from candidate", nativeQuery = true)
    Optional<Long> findMaxCandidateNo();

    class QueryGeneration extends BaseRepository.QueryGeneration {

        public static Specification<Candidate> search(SearchDto searchDto) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(statusPredicate(cb, root, searchDto.getStatus()));
                predicates.add(idPredicate(cb, root, searchDto.getId()));
                predicates.add(likePredicate(cb, root, "city", searchDto.getCity()));
                predicates.add(likePredicate(cb, root, "district", searchDto.getDistrict()));
                predicates.add(likePredicate(cb, root, "fullname", searchDto.getFullname()));
                predicates.add(equalBooleanPredicate(cb, root, "interview", searchDto.getInterview()));
                predicates.add(equalPredicate(cb, root, "candidateNo", searchDto.getCandidateNo()));
                predicates.add(gePredicate(cb, root, "minSalary", searchDto.getMinSalary()));
                predicates.add(geDatePredicate(cb, root, "createdDate", searchDto.getDateInterval()));
                resolveUser(predicates, query, root, cb, searchDto);
                resolveProperty(predicates, query, root, cb, searchDto);
                resolveSearchText(predicates, query, root, cb, searchDto);
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }

        public static void resolveSearchText(List<Predicate> predicates, CriteriaQuery<?> query, Root<?> root, CriteriaBuilder cb, SearchDto searchDto) {
            if (!StringUtils.isEmpty(searchDto.getSearchText())) {
                List<Predicate> orPredicates = getPredicateArray();
                orPredicates.add(likePredicate(cb, root, "title", searchDto.getSearchText()));
                orPredicates.add(likePredicate(cb, root, "fullname", searchDto.getFullname()));
                resolvePropertySearchText(orPredicates, query, root, cb, searchDto);
                predicates.add(cb.or(orPredicates.toArray(new Predicate[orPredicates.size()])));
            }
        }

        private static void resolveUser(List<Predicate> predicates, CriteriaQuery<?> query, Root<Candidate> root, CriteriaBuilder cb, SearchDto searchDto) {
            if (!StringUtils.isEmpty(searchDto.getUserId())) {
                Subquery<User> subQuery = query.subquery(User.class);
                Root<User> subRoot = subQuery.from(User.class);
                subQuery.select(subRoot);
                List<Predicate> subQueryPredicates = getPredicateArray();
                subQueryPredicates.add(cb.equal(subRoot.get("identifier"), searchDto.getUserId()));
                subQueryPredicates.add(cb.equal(subRoot.get("identifier"), root.join("user")));
                subQuery.where(subQueryPredicates.toArray(new Predicate[]{}));
                predicates.add(cb.exists(subQuery));
            }
        }

        private static void resolveProperty(List<Predicate> predicates, CriteriaQuery<?> query, Root<Candidate> root, CriteriaBuilder cb, SearchDto searchDto) {
            if (!CollectionUtils.isEmpty(searchDto.getProperties())) {
                searchDto.getProperties().keySet().forEach(p -> {
                    Subquery<CandidateProperty> subQuery = query.subquery(CandidateProperty.class);
                    Root<CandidateProperty> subRoot = subQuery.from(CandidateProperty.class);
                    subQuery.select(subRoot);
                    List<Predicate> subQueryPredicates = getPredicateArray();
                    subQueryPredicates.add(equalPredicate(cb, subRoot, "searchText", p));
                    subQueryPredicates.add(inPredicate(cb, subRoot, "value", Arrays.stream(searchDto.getProperties().get(p).split(Constants.comma)).collect(Collectors.toSet())));
                    subQueryPredicates.add(cb.equal(subRoot.join("candidate").get("identifier"), root.get("identifier")));
                    subQuery.where(subQueryPredicates.toArray(new Predicate[]{}));
                    predicates.add(cb.exists(subQuery));
                });
            }
        }

        private static void resolvePropertySearchText(List<Predicate> predicates, CriteriaQuery<?> query, Root<?> root, CriteriaBuilder cb, SearchDto searchDto) {
            if (!StringUtils.isEmpty(searchDto.getSearchText())) {
                Subquery<CandidateProperty> subQuery = query.subquery(CandidateProperty.class);
                Root<CandidateProperty> subRoot = subQuery.from(CandidateProperty.class);
                subQuery.select(subRoot);
                List<Predicate> subQueryPredicates = getPredicateArray();
                subQueryPredicates.add(cb.equal(subRoot.join("candidate").get("identifier"), root.get("identifier")));
                subQueryPredicates.add(likePredicate(cb, subRoot, "value", searchDto.getSearchText()));
                subQuery.where(subQueryPredicates.toArray(new Predicate[]{}));
                predicates.add(cb.exists(subQuery));
            }
        }
    }
}
