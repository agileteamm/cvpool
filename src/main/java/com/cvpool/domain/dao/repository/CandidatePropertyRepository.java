package com.cvpool.domain.dao.repository;

import com.cvpool.common.dao.repository.BaseRepository;
import com.cvpool.domain.dao.entity.Candidate;
import com.cvpool.domain.dao.entity.CandidateProperty;

import java.util.List;
import java.util.Optional;

public interface CandidatePropertyRepository extends BaseRepository<CandidateProperty, String> {

    Optional<List<CandidateProperty>> findByCandidate(Candidate candidate);
}
