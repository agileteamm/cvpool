package com.cvpool.domain.dao.entity;

import com.cvpool.common.dao.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Data
@Entity(name = "CANDIDATE_PROPERTY")
public class CandidateProperty extends BaseEntity {
    private String searchText;
    private String value;
    @ManyToOne
    private Property property;
    @ManyToOne
    private Candidate candidate;
}