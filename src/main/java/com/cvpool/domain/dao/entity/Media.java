package com.cvpool.domain.dao.entity;

import com.cvpool.common.dao.entity.BaseEntity;
import com.cvpool.common.dao.entity.IdEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "media")
public class Media extends BaseEntity {
    private String fileUrl;
    @Type(type = "text")
    private String media;
}