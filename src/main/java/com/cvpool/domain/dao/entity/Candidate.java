package com.cvpool.domain.dao.entity;

import com.cvpool.common.dao.entity.BaseAuditEntity;
import com.cvpool.common.enumeration.EntityStatus;
import com.cvpool.common.util.Constants;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Candidate extends BaseAuditEntity {
    private static final long serialVersionUID = -6208248864071830402L;

    private Long candidateNo;
    @Enumerated(EnumType.STRING)
    private EntityStatus status;
    private String title;
    @Type(type = "text")
    private String description;
    private String searchText;
    private String city;
    private String district;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "media_id")
    private Media media;
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
    private String fullname;
    private Boolean interview;
    private Integer minSalary;

    public String getUrl() {
        return "aday" + Constants.slash + this.getIdentifier();
    }

    public String getLocation() {
        return this.getCity() + Constants.slash + this.getDistrict();
    }

    public String getMediaPath() {
        return Constants.slash + LocalDate.from(this.media.getCreatedDate()).getYear() +
                Constants.slash + LocalDate.from(this.media.getCreatedDate()).getMonthValue() +
                Constants.slash + this.identifier;
    }
}
