package com.cvpool.domain.dto;

import com.cvpool.common.enumeration.EntityStatus;
import com.cvpool.common.enumeration.PropertyType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class PropertyDto implements Serializable {
	private static final long serialVersionUID = 3809104814337239544L;
	private EntityStatus status;
	private PropertyType type;
	private String name;
	private Boolean searchParam;
	private Boolean mandatory;
	private Long orderId;
	private String parentId;
	private String categoryId;

}
