package com.cvpool.domain.dto;

import com.cvpool.common.enumeration.EntityStatus;
import lombok.Builder;
import lombok.Data;

import java.util.Map;
import java.util.Set;

@Data
@Builder
public class SearchDto {
    private int page;
    private int size;
    private String sort;
    private String direction;
    private String id;
    private Long candidateNo;
    private String categorySearchText;
    private Set<String> categoryIds;
    private String searchText;
    private EntityStatus status;
    private Integer dateInterval;
    private String city;
    private String district;
    private String userId;
    private String fullname;
    private Boolean interview;
    private Integer minSalary;
    private Map<String, String> properties;
}
