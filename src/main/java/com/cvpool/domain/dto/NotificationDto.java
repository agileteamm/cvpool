package com.cvpool.domain.dto;

import com.cvpool.common.dao.entity.BaseEntity;
import com.cvpool.common.enumeration.EntityStatus;
import com.cvpool.common.enumeration.NotificationType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NotificationDto<T extends BaseEntity> {
    private T entity;
    private NotificationType type;
    private EntityStatus oldStatus;
    private EntityStatus status;
}
