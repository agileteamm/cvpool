package com.cvpool.domain.dto;

import java.io.Serializable;

import com.cvpool.common.enumeration.PropertyType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CandidatePropertyDto implements Serializable {
    private static final long serialVersionUID = 3809104814337239678L;
    private String identifier;
    private PropertyType type;
    private String name;
    private String value;
    private String propertyId;
}
