package com.cvpool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@EnableCaching
@SpringBootApplication(scanBasePackages = {"com.cvpool"})
@EnableConfigurationProperties
public class CVPoolApplication {
	public static void main(String[] args) {
		SpringApplication.run(CVPoolApplication.class, args);
	}
}

