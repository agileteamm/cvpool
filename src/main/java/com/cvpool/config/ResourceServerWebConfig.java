package com.cvpool.config;

import com.cvpool.common.interceptor.RequestInterceptor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mobile.device.DeviceHandlerMethodArgumentResolver;
import org.springframework.mobile.device.DeviceResolverHandlerInterceptor;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

import static com.cvpool.common.util.Constants.FILE_PREFIX;

@Data
@Configuration
@ConfigurationProperties(prefix = "system.parameter.file")
public class ResourceServerWebConfig extends WebMvcConfigurationSupport {


    @Autowired
    private RequestInterceptor requestInterceptor;

    String root;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");

        //Other static resources
        registry.addResourceHandler("static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("admin/**").addResourceLocations("classpath:/static/admin/");
        registry.addResourceHandler("js/**").addResourceLocations("classpath:/static/js/");
        registry.addResourceHandler("css/**").addResourceLocations("classpath:/static/css/");
        registry.addResourceHandler("img/**").addResourceLocations("classpath:/static/img/");
        registry.addResourceHandler("fonts/**").addResourceLocations("classpath:/static/fonts/");
        registry.addResourceHandler(FILE_PREFIX + "/**").addResourceLocations("file:" + root);
    }

    @Bean
    public DeviceResolverHandlerInterceptor deviceResolverHandlerInterceptor() {
        return new DeviceResolverHandlerInterceptor();
    }

    @Bean
    public DeviceHandlerMethodArgumentResolver deviceHandlerMethodArgumentResolver() {
        return new DeviceHandlerMethodArgumentResolver();
    }

    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestInterceptor);
        registry.addInterceptor(deviceResolverHandlerInterceptor());
    }

    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(deviceHandlerMethodArgumentResolver());
    }
}