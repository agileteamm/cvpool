package com.cvpool.config;

import com.cvpool.domain.service.notification.NotificationUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class StaticInitializer {

    @Value("${system.parameter.baseUrl}")
    private String baseUrl;
    @Value("${system.parameter.email}")
    private String email;

    @PostConstruct
    void init() {
        NotificationUtils.baseUrl = baseUrl;
        NotificationUtils.email = email;
    }
}